package com.weather.forecast.config;

/**
 * System Properties
 */
public class Configurations {
    public static final String API_KEY = "1b6e7f5e19b857945c1c19f2ab3ec54d";
    public static final String DEFAULT_START_TIME = "10:00:00";
    public static final String DEFAULT_END_TIME = "18:00:00";
    public static final String URL = "http://api.openweathermap.org/data/2.5/forecast";
    public static final String DESCRIPTION = "Weather forecast for three days";
}
