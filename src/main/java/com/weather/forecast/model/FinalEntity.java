package com.weather.forecast.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class FinalEntity {
    private int status;
    private String message;
    private int responseTime;
    private String description;
    private List<WeatherEntity> data = new ArrayList<>();
}
